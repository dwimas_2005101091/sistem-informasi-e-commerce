<?php
session_start();
include "../include/functions.php";
include "../include/session.php";


if (!isset($_GET["id_produk"])) {
    header("Location: ../../");
}
if (!isset($_SESSION['login'])) {
    header("Location: ../login.php");
    exit;
}

$id_produk =  RemoveSpecialChar((int)$_GET['id_produk']);

if (isset($_SESSION['login'])) {
    $userId = $_COOKIE["id"];
}

$produk = query("SELECT * FROM produk WHERE id_produk=$id_produk")[0];
$keranjang = mysqli_query($conn, "SELECT * FROM keranjang WHERE id_user ='$userId' AND id_produk = '$id_produk' ");
$result = mysqli_num_rows($keranjang);
if ($keranjang = null) {
    header("Location: ../../");
}
if ($result == 0) {
    header("Location: ../../");
}




?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $produk["nama_barang"]; ?></title>
    <link rel="stylesheet" href="../../style/style-product.css">
    <link rel="icon" type="image/x-icon" href="../../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../../script.js"></script>
</head>
<?php
if (isset($_POST["keranjang_Hapus"])) {
    if (!isset($_SESSION['login'])) {
        echo '<div><script>';
        echo "keranjangLogin();";
        echo '</script></div>';
    } else if (keranjangHapus($id_produk, $userId) > 0) {
        echo '<div><script>';
        echo "keranjangOke();";
        echo '</script></div>';
    } else {
        echo '<div><script>';
        echo "keranjangGagal();";
        echo '</script></div>';
    }
}
if (isset($_POST['beli'])) {
    if (!isset($_SESSION['login'])) {
        echo '<div><script>';
        echo "keranjangLogin();";
        echo '</script></div>';
    }
}

?>

<body>
    <!-- BAGIAN NAVIGASI ATAS -->
    <nav>
        <div class="logo-center">
            <div class="fav">
                <a href="../../">Enryu<span>.</span>co</a>
            </div>
            <div class="ham">
                <i class="ham-button fa-solid fa-bars"></i>
            </div>
        </div>
        <div class="navsub subright">
            <ul>
                <li>
                    <div class="searchBar">
                        <form action="" method="GET">
                            <input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
                                                                                        echo $_GET['keyword'];
                                                                                    } ?>">
                            <button>
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </form>
                    </div>
                </li>
                <li><a href="../katalog">KATALOG</a></li>
                <?php if (isset($_SESSION["login"])) { ?>
                    <?php if ($_SESSION["level"] === "SU_ADMIN") { ?>
                        <li><a href="../../admin/admin.php">ADMIN DASHBOARD</a></li>
                    <?php } else { ?>
                        <li><a href="../keranjang">KERANJANG</a></li>
                        <li><a href="../pesanan">PESANAN</a></li>
                        <li><a href="../user/user">AKUN</a></li>
                    <?php } ?>
                    <li><a href="../logout.php">LOG OUT</a></li>
                <?php  } else if (!isset($_SESSION["login"])) { ?>

                    <li><a href="../login">SIGN IN</a></li>
                    <li><a href="../sign-up">SIGN UP</a></li>

                <?php  } ?>
            </ul>
        </div>
    </nav>
    <div class="spacer"></div>
    <!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
    <div class="wrapper-detail">
        <div class="product-detail">
            <div class="slider-outer detail-product">
                <i class="fa-solid  fa-chevron-left prev detail-product"></i>
                <div class="slidder-inner detail-product">
                    <div class="product-detail-img">
                        <img src="../../img/<?= $produk["gambar1"]; ?>" alt="sepatu-keren" title="sepatu keren" class="active fade first">
                        <img src="../../img/<?= $produk["gambar2"]; ?>" alt="sepatu-keren" title="sepatu keren" class="fade">
                        <img src="../../img/<?= $produk["gambar3"]; ?>" alt="sepatu-keren" title="sepatu keren" class="fade">
                        <img src="../../img/<?= $produk["gambar4"]; ?>" alt="sepatu-keren" title="sepatu keren" class="fade end">
                    </div>
                </div>
                <i class="fa-solid fa-chevron-right next detail-product"></i>
            </div>
            <div class="product-desc">
                <div class="product-title">
                    <h3><?= $produk["nama_barang"]; ?></h3>
                </div>
                <div class="product-price">
                    <p><?= rupiah($produk["harga"]); ?></p>
                </div>
                <div class="product-caption">
                    <p><?= $produk["desc_short"]; ?></p>
                </div>
                <div class="product-size">
                    <?php if (!isset($_SESSION['login'])) { ?>
                        <form action="" method="POST" id="Form<?php echo "lord"; ?>">
                        <?php } else { ?>
                            <form action="../order.php" method="POST" id="Form">
                            <?php } ?>
                            <input type="hidden" name="id_produk" value="<?= $produk['id_produk'] ?>">
                            <label for="ukuran" class="label-ukuran">Ukuran :</label> <br>
                            <select name="ukuran" id="ukuran" required>
                                <option value="" disabled selected>Select Size</option>
                                <option value="34">34</option>
                                <option value="35">35</option>
                                <option value="36">36</option>
                                <option value="37">37</option>
                                <option value="38">38</option>
                                <option value="39">39</option>
                                <option value="40">40</option>
                                <option value="41">41</option>
                                <option value="42">42</option>
                            </select>
                            <label for="jumlah">Jumlah item :</label>
                            <input type="number" name="jumlah" id="jumlah" placeholder="xxx" min="1" max="100" required>
                            <div class="product-buy">
                                <button type="submit" name="beli" class="beli-btn">Order Now</button>
                            </div>
                            </form>
                            <form action="" method="post">
                                <div class="keranjang">
                                    <button type="submit" name="keranjang_Hapus">Hapus dari Keranjang</button>
                                </div>
                            </form>
                </div>
                <div class="detail">
                    <h4>Deskripsi :</h4>
                    <p><?= $produk["desc_long"]; ?></p>
                </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footer-botom">
            <div class="foot foot-left">
                <p>© 2022 ENRYU CO</p>
            </div>
            <div class="foot foot-center">
                <a href="#">ABOUT</a>
                <a href="#">TERMS AND CONDITIONS</a>
                <a href="#">PRIVACY</a>
            </div>
            <div class="foot foot-right">
                <p>Created by Dwimas Budi Sulistyo</p>
            </div>
        </div>
    </footer>
    <script>
        document.querySelector('#Form').addEventListener('submit', function(e) {
            var form = this;

            e.preventDefault(); // <--- prevent form from submitting

            swal.fire({
                title: 'Kamu yakin?',
                text: "Ingin membeli produk ini!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oke',
                dangerMode: true,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Berhasil di pesan!', '', 'success').then(function() {
                        form.submit();
                    });
                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    </script>
</body>

</html>