<?php 
session_start();
session_unset();
session_destroy();
$_SESSION = [];
$expire = 2 * 30 * 24 * 3600;
//menghapus cookie
setcookie('id', '', time()-$expire, '/');
setcookie('key', '', time() -$expire, '/');
setcookie('level', '', time() -$expire, '/');
setcookie('key_pass', '', time() -$expire, '/');
header("Location: login.php");

exit;
?>