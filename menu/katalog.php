<?php
session_start();
include "../include/functions.php";
include "../include/session.php";
// include "headers.php";

$produk = query("SELECT * FROM produk ORDER BY id_produk DESC");

if (isset($_GET['keyword'])) {
    $produk = cari($_GET['keyword']);
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>katalog</title>
    <link rel="stylesheet" href="../style/style.css">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>

<body>
    <!-- BAGIAN NAVIGASI ATAS -->
    <nav>
        <div class="logo-center">
            <div class="fav">
                <a href="../">Enryu<span>.</span>co</a>
            </div>
            <div class="ham">
                <i class="ham-button fa-solid fa-bars"></i>
            </div>
        </div>
        <div class="navsub subright">
            <ul>
                <li>
                    <div class="searchBar">
                        <form action="" method="GET">
                            <input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
                                                                                        echo $_GET['keyword'];
                                                                                    } ?>">
                            <button>
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </form>
                    </div>
                </li>
                <li><a href="katalog">KATALOG</a></li>
                <?php if (isset($_SESSION["login"])) { ?>
                    <?php if ($_SESSION["level"] === "SU_ADMIN") { ?>
                        <li><a href="../admin/admin.php">ADMIN DASHBOARD</a></li>
                    <?php } else { ?>
                        <li><a href="keranjang">KERANJANG</a></li>
                        <li><a href="pesanan">PESANAN</a></li>
                        <li><a href="../user/user">AKUN</a></li>
                    <?php } ?>
                    <li><a href="logout.php">LOG OUT</a></li>
                <?php  } else if (!isset($_SESSION["login"])) { ?>

                    <li><a href="login">SIGN IN</a></li>
                    <li><a href="sign-up">SIGN UP</a></li>
                <?php  } ?>
            </ul>
        </div>
    </nav>
    <div class="spacer"></div>
    <!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
    <div class="headline">
        <h2>Daftar Produk</h2>
    </div>
    <main>
        <?php foreach ($produk as $row) : ?>
            <a href="produk/<?= $row["id_produk"] ?>" data-aos="zoom-in-up">
                <section>
                    <div class="product-img">
                        <img src="../img/<?= $row["gambar1"] ?>" alt="sepatu keren">
                    </div>
                    <div class="caption">
                        <h5><?= $row["nama_barang"]; ?></h5>
                        <p><?= rupiah($row["harga"]); ?></p>
                    </div>
                </section>
            </a>
        <?php endforeach; ?>
    </main>
    <footer>
        <div class="footer-botom">
            <div class="foot foot-left">
                <p>© 2022 ENRYU CO</p>
            </div>
            <div class="foot foot-center">
                <a href="#">ABOUT</a>
                <a href="#">TERMS AND CONDITIONS</a>
                <a href="#">PRIVACY</a>
            </div>
            <div class="foot foot-right">
                <p>Created by Dwimas Budi Sulistyo</p>
            </div>
        </div>
    </footer>
    <script src="script.js"></script>
    <script>
        AOS.init({
            once: true,
        });
    </script>
</body>

</html>