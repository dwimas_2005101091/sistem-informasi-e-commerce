<?php
ob_start();
session_start();
include "../include/functions.php";
include "../include/session.php";
// include "headers.php";
// cek cookie 
if (isset($_SESSION["login"])) {
	if ($_SESSION["level"] == "SU_ADMIN") {
		header("Location: ../admin/admin.php");
	} else {
		header("Location: katalog.php");
	}
	exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="../style/login-style.css">
	<link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
	<script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="../script.js"></script>
</head>
<?php
if (isset($_POST['login'])) {
	global $conn;

	$username =  mysqli_real_escape_string($conn, strtolower(stripslashes($_POST["username"])));
	$password = mysqli_real_escape_string($conn, $_POST["password"]);

	$result = mysqli_query($conn, "SELECT * FROM user WHERE username = '$username' ");

	if (mysqli_num_rows($result) === 1) {
		//cek password
		$row = mysqli_fetch_assoc($result);
		if (password_verify($password, $row['password'])) {
			// set session
			$_SESSION["login"] = true;
			$_SESSION["level"] = $row["level"];
			// cek remember me
			if (isset($_POST['remember'])) {
				//buat cookie
				//
				$expire = 1 * 30 * 24 * 3600;
				setcookie('id', $row['id_user'], time() + $expire, '/');
				setcookie('level', $row['level'], time() + $expire, '/');
				setcookie('key_pass', $row['password'], time() + $expire, '/');
				setcookie('key', hash('sha256', $row['username']), time() + $expire, '/');
			}

			if ($row['level'] == 'SU_ADMIN') {
				echo '<div><script>';
				echo "myFnAdmin();";
				echo '</script></div>';
			} else if ($row['level'] == 'USER') {
				echo '<div><script>';
				echo 'myFnUser();';
				echo '</script></div>';
			}

			// header("location:admin.php");
			// exit;
		} else {
			echo '<div><script>myFnGagalPass()</script></div>';
		}
	} else {
		echo '<div><script>myFnGagal()</script></div>';
	}
}
?>

<body>
	<!-- BAGIAN NAVIGASI ATAS -->
	<nav>
		<div class="logo-center">
			<div class="fav">
				<a href="../">Enryu<span>.</span>co</a>
			</div>
			<div class="ham">
				<i class="ham-button fa-solid fa-bars"></i>
			</div>
		</div>
		<div class="navsub subright">
			<ul>
				<li>
					<div class="searchBar">
						<form action="katalog" method="GET">
							<input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
																						echo $_GET['keyword'];
																					} ?>">
							<button>
								<i class="fa-solid fa-magnifying-glass"></i>
							</button>
						</form>
					</div>
				</li>
				<li><a href="katalog">KATALOG</a></li>
				<li><a href="login">SIGN IN</a></li>
				<li><a href="sign-up">SIGN UP</a></li>
			</ul>
		</div>
	</nav>
	<div class="spacer"></div>
	<!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
	<main>
		<div class="wrapper-login">
			<div class="sideImage">
				<img src="https://img.freepik.com/free-vector/active-recreation-seamless-pattern_1284-7323.jpg?w=740&t=st=1666152315~exp=1666152915~hmac=83f8e5401f767d632dfd9c4ee4f0b7a5936f563ccac4b177f0bbbf10ddb0977e" alt="pattern-img">
			</div>
			<div class="login-page">
				<div class="main-logo">
					<img src="../assets/logo-sepatu.png" alt="logo-sepatu">
					<div class="text-logo">
						<h1>Enryu<span>.</span>co</h1>
					</div>
					<h3>Sign in</h3>
				</div>
				<form action="" method="POST">
					<label for="username">Username :<span>*</span></label>
					<input type="username" id="username" name="username" placeholder="username" required>
					<label for="password">Password :<span>*</span></label>
					<input type="password" id="password" name="password" placeholder="******" required>
					<input type="checkbox" name="remember" id="remember" checked hidden>
					<div class="button-daftar">
						<a href="sign-up.php">Sign up</a>
						<input type="submit" value="Sign in" name="login">
					</div>
				</form>
			</div>
		</div>
	</main>
	<div class="spacer"></div>
	<footer>
		<div class="footer-botom">
			<div class="foot foot-left">
				<p>© 2022 ENRYU CO</p>
			</div>
			<div class="foot foot-center">
				<a href="#">ABOUT</a>
				<a href="#">TERMS AND CONDITIONS</a>
				<a href="#">PRIVACY</a>
			</div>
			<div class="foot foot-right">
				<p>Created by Dwimas Budi Sulistyo</p>
			</div>
		</div>
	</footer>

</body>

</html>