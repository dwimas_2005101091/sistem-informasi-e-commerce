<?php
include "../include/functions.php";
include "../include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign up</title>
	<link rel="stylesheet" type="text/css" href="../style/sign-up-style.css">
	<link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
	<script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="../script.js"></script>
</head>
<?php
if (isset($_POST['register'])) {
	if (registrasi($_POST) > 0) {
		echo '<div><script>';
		echo "registrasiOke();";
		echo '</script></div>';
	} else {
		echo mysqli_error($conn);
	}
}
?>

<body>
	<!-- BAGIAN NAVIGASI ATAS -->
	<nav>
		<div class="logo-center">
			<div class="fav">
				<a href="../">Enryu<span>.</span>co</a>
			</div>
			<div class="ham">
				<i class="ham-button fa-solid fa-bars"></i>
			</div>
		</div>
		<div class="navsub subright">
			<ul>
				<li>
					<div class="searchBar">
						<form action="katalog" method="GET">
							<input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
																						echo $_GET['keyword'];
																					} ?>">
							<button>
								<i class="fa-solid fa-magnifying-glass"></i>
							</button>
						</form>
					</div>
				</li>
				<li><a href="katalog">KATALOG</a></li>
				<li><a href="login">SIGN IN</a></li>
				<li><a href="sign-up">SIGN UP</a></li>
			</ul>
		</div>
	</nav>
	<div class="spacer"></div>
	<!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
	<main>
		<div class="wrapper-signup">
			<div class="signup-page">
				<div class="main-logo">
					<!-- <img src="assets/logo-sepatu.png"> -->
					<div class="text-logo">
						<h1>Enryu<span>.</span>co</h1>
					</div>
				</div>
				<h3>Sign up</h3>
				<form action="" method="POST">
					<label for="nama">Nama Lengkap :<span>*</span></label>
					<input type="text" id="nama" name="nama" placeholder="Dwimas Budi" required>
					<label for="username">Username :<span>*</span></label>
					<input type="text" id="username" name="username" placeholder="username" required>
					<label for="password">Password :<span>*</span></label>
					<input type="password" id="password" name="password" placeholder="********" required>
					<label for="email">Email :<span>*</span></label>
					<input type="email" id="email" name="email" placeholder="email@pribadi.com">
					<div class="container">
						<input type="submit" value="Sign up" class="daftar" id="daftar" name="register">
					</div>
				</form>
			</div>
			<div class="sideImage">
				<img src="https://img.freepik.com/free-vector/active-recreation-seamless-pattern_1284-7323.jpg?w=740&t=st=1666152315~exp=1666152915~hmac=83f8e5401f767d632dfd9c4ee4f0b7a5936f563ccac4b177f0bbbf10ddb0977e" alt="pattern-img">
			</div>
		</div>
	</main>
	<div class="spacer"></div>
	<footer>
		<div class="footer-botom">
			<div class="foot foot-left">
				<p>© 2022 ENRYU CO</p>
			</div>
			<div class="foot foot-center">
				<a href="#">ABOUT</a>
				<a href="#">TERMS AND CONDITIONS</a>
				<a href="#">PRIVACY</a>
			</div>
			<div class="foot foot-right">
				<p>Created by Dwimas Budi Sulistyo</p>
			</div>
		</div>
	</footer>
	<script src="script.js"></script>
</body>

</html>