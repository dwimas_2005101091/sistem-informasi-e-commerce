<?php
session_start();
include "../include/functions.php";
include "../include/session.php";

if (isset($_SESSION['login'])) {
    $userId = $_COOKIE["id"];
}
if (!isset($_SESSION['login'])) {
    header("Location: login.php");
    exit;
}
$id  = $_COOKIE['id'];

$transaksi = query("SELECT id_transaksi, id_user, transaksi.id_produk, nama_barang, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk WHERE id_user = '$id' ORDER BY id_transaksi DESC");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pembelian</title>
    <link rel="stylesheet" href="../style/pesanan-style.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../../script.js"></script>
</head>

<body>
    <!-- BAGIAN NAVIGASI ATAS -->
    <nav>
        <div class="logo-center">
            <div class="fav">
                <a href="../">Enryu<span>.</span>co</a>
            </div>
            <div class="ham">
                <i class="ham-button fa-solid fa-bars"></i>
            </div>
        </div>
        <div class="navsub subright">
            <ul>
                <li>
                    <div class="searchBar">
                        <form action="katalog" method="GET">
                            <input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
                                                                                        echo $_GET['keyword'];
                                                                                    } ?>">
                            <button>
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </form>
                    </div>
                </li>
                <li><a href="katalog">KATALOG</a></li>
                <?php if (isset($_SESSION["login"])) { ?>
                    <?php if ($_SESSION["level"] === "SU_ADMIN") { ?>
                        <li><a href="../admin.php">ADMIN DASHBOARD</a></li>
                    <?php } else { ?>
                        <li><a href="keranjang">KERANJANG</a></li>
                        <li><a href="#">PESANAN</a></li>
                        <li><a href="../user/user">AKUN</a></li>
                    <?php } ?>
                    <li><a href="logout.php">LOG OUT</a></li>
                <?php  } else if (!isset($_SESSION["login"])) { ?>

                    <li><a href="../login">SIGN IN</a></li>
                    <li><a href="../sign-up">SIGN UP</a></li>

                <?php  } ?>
            </ul>
        </div>
    </nav>
    <div class="spacer"></div>
    <!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
    <main>
        <?php foreach ($transaksi as $row) : ?>
            <div class="pesanan-detail">
                <div class="detail-img">
                    <img src="../img/<?= $row['gambar1'] ?>" alt="sepatu keran">
                    <div class="pesanan-name">
                        <p><?= $row['nama_barang'] ?></p>
                    </div>
                </div>
                <div class="row pesanan-ukuran">
                    <p>Size : <?= $row['ukuran'] ?></p>
                </div>
                <div class="row pesanan-jumlah">
                    <p>jumlah : <?= $row['jumlah_pembelian'] ?></p>
                </div>
                <div class="row pesanan-harga">
                    <p>Harga :</p>
                    <p><?= rupiah($row['harga']) ?></p>
                </div>
                <div class="row pesanan-total">
                    <p>Total :</p>
                    <p><?= rupiah($row['total_bayar']) ?></p>
                </div>
                <div class="row pesanan-aksi">
                    <?php if ($row['info_status'] == "BELUM BAYAR") { ?>
                        <a href="pembelian.php?id=<?= $row['id_transaksi'] ?>">BAYAR</a>
                    <?php } else if ($row['info_status'] == "LUNAS") { ?>
                        <a href="pembelian.php?id=<?= $row['id_transaksi'] ?>">PROSES</a>
                    <?php } else { ?>
                        <a href="pembelian.php?id=<?= $row['id_transaksi'] ?>"><?= $row['info_status'] ?></a>
                    <?php } ?>
                </div>
            </div>
        <?php endforeach; ?>
    </main>
    <footer>
        <div class="footer-botom">
            <div class="foot foot-left">
                <p>© 2022 ENRYU CO</p>
            </div>
            <div class="foot foot-center">
                <a href="#">ABOUT</a>
                <a href="#">TERMS AND CONDITIONS</a>
                <a href="#">PRIVACY</a>
            </div>
            <div class="foot foot-right">
                <p>Created by Dwimas Budi Sulistyo</p>
            </div>
        </div>
    </footer>
</body>

</html>