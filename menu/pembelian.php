<?php
session_start();
include "../include/functions.php";
include "../include/session.php";

if (isset($_SESSION['login'])) {
    $userId = $_COOKIE["id"];
}
if (!isset($_SESSION['login']) || !isset($_GET['id'])) {
    header("Location: ../menu/login.php");
    exit;
}

$user = query("SELECT * FROM user where id_user='$userId'")[0];
$transaksi_id =  RemoveSpecialChar((int)$_GET['id']);

$transaksi = query("SELECT id_transaksi, id_user, transaksi.id_produk, nama_barang, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status, keterangan
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk WHERE id_user = '$userId' AND transaksi.id_transaksi ='$transaksi_id'")[0];

if ($transaksi == null) {
    header("Location: pesanan.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pembelian</title>
    <link rel="stylesheet" href="../style/style-pembelian.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>
<?php
if (isset($_POST['batal'])) {
    if (batalTransaksi($transaksi_id, $userId) > 0) {
        echo '<div><script>';
        echo "berhasilBatal();";
        echo '</script></div>';
    }
}
if (isset($_POST['bayar'])) {
    if ($user['alamat'] == NULL || $user['no_handphone'] == NULL) {
        echo '<div><script>';
        echo "pembelianUbah();";
        echo '</script></div>';
    } else {
        if (KirimBukti($transaksi_id, $userId) > 0) {
            echo '<div><script>';
            echo "berhasilBayar();";
            echo '</script></div>';
        }
    }
}

?>

<body>
    <!-- BAGIAN NAVIGASI ATAS -->
    <nav>
        <div class="logo-center">
            <div class="fav">
                <a href="../">Enryu<span>.</span>co</a>
            </div>
            <div class="ham">
                <i class="ham-button fa-solid fa-bars"></i>
            </div>
        </div>
        <div class="navsub subright">
            <ul>
                <li>
                    <div class="searchBar">
                        <form action="katalog" method="GET">
                            <input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
                                                                                        echo $_GET['keyword'];
                                                                                    } ?>">
                            <button>
                                <i class="fa-solid fa-magnifying-glass"></i>
                            </button>
                        </form>
                    </div>
                </li>
                <li><a href="katalog">KATALOG</a></li>
                <?php if (isset($_SESSION["login"])) { ?>
                    <?php if ($_SESSION["level"] === "SU_ADMIN") { ?>
                        <li><a href="../admin/admin.php">ADMIN DASHBOARD</a></li>
                    <?php } else { ?>
                        <li><a href="keranjang">KERANJANG</a></li>
                        <li><a href="pesanan.php">PESANAN</a></li>
                        <li><a href="../user/user">AKUN</a></li>
                    <?php } ?>
                    <li><a href="logout.php">LOG OUT</a></li>
                <?php  } else if (!isset($_SESSION["login"])) { ?>

                    <li><a href="login">SIGN IN</a></li>
                    <li><a href="sign-up">SIGN UP</a></li>

                <?php  } ?>
            </ul>
        </div>
    </nav>
    <div class="spacer"></div>
    <!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
    <div class="wrapper-pembelian">
        <div class="pembelian-detail">
            <div class="invoice">
                <h2>INVOICE#<?= $transaksi['id_transaksi'] ?></h2>
                <div class="info-rekening">
                    <div class="detail-rekening">
                        <div class="rekening">
                            <h4>Daftar rekening pembayaran</h4>
                            <p>Bank Mandiri</p>
                            <p>a/n PT ENRYU.CO</p>
                            <p style="text-decoration: underline;">No. Rekening : 1300010441769</p>
                        </div>
                        <div class="rekening">
                            <p>Bank BCA</p>
                            <p>a/n PT ENRYU.CO</p>
                            <p style="text-decoration: underline;">No. Rekening : 775034027</p>
                        </div>
                    </div>
                    <div class="instruksi-pembayaran">
                        <h4>Instruksi pembayaran</h4>
                        <p>Mohon dituliskan berita:</p>
                        <p>INVOICE-<?= $transaksi['id_transaksi'] ?></p>
                        <p>pada kolom berita transfer.</p>
                        <p>Silahkan transfer dengan jumlah :</p>
                        <p><?= rupiah($transaksi['total_bayar']) ?></p>
                        <p class="red">STATUS : <?= $transaksi['info_status'] ?></p>
                    </div>
                </div>
                <hr>
                <div class="invoice-desc">
                    <div class="li-desc">
                        <p>Detail</p>
                        <p>Harga</p>
                    </div>
                    <div class="li-desc">
                        <p><?= $transaksi['nama_barang'] ?></p>
                        <p><?= rupiah($transaksi['harga']) ?></p>
                    </div>
                    <div class="li-desc">
                        <p>jumlah</p>
                        <p>x <?= $transaksi['jumlah_pembelian'] ?></p>
                    </div>
                    <div class="li-desc">
                        <p>Total</p>
                        <p><?= rupiah($transaksi['total_bayar']) ?></p>
                    </div>
                </div>
                <hr>
                <?php if ($transaksi['info_status'] == 'BELUM BAYAR') { ?>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="file-upload">
                            <label for="">
                                Upload Bukti Pembayaran :
                                <input type="file" name="gambar">
                            </label>
                        </div>
                        <div class="btn-submit">
                            <button class="submit" name="batal">Batalkan Pesanan</button>
                            <button class="submit" name="bayar">Bayar</button>
                        </div>
                    </form>
                <?php } else if ($transaksi['info_status'] == 'DI KIRIM') { ?>
                    <p class="red">Keterangan : <?= $transaksi['keterangan'] ?></p>
                    <form action="terima.php" method="POST" id="Form">
                        <input type="hidden" name="id" value="<?= $transaksi['id_transaksi'] ?>" readonly>
                        <div class="btn-submit">
                            <button type="submit" class="submit" name="terima">Pesanan Diterima</button>
                        </div>
                    </form>
                <?php } else { ?>
                    <p class="red">Keterangan : <?= $transaksi['keterangan'] ?></p>
                <?php } ?>
            </div>
        </div>
    </div>
    </div>

    <footer>
        <div class="footer-botom">
            <div class="foot foot-left">
                <p>© 2022 ENRYU CO</p>
            </div>
            <div class="foot foot-center">
                <a href="#">ABOUT</a>
                <a href="#">TERMS AND CONDITIONS</a>
                <a href="#">PRIVACY</a>
            </div>
            <div class="foot foot-right">
                <p>Created by Dwimas Budi Sulistyo</p>
            </div>
        </div>
    </footer>
    <script>
        document.querySelector('#Form').addEventListener('submit', function(e) {
            var form = this;

            e.preventDefault(); // <--- prevent form from submitting

            swal.fire({
                title: 'Kamu yakin?',
                text: "Produk ini sudah kamu terima!",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oke',
                // dangerMode: true,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    Swal.fire('Terima produk!', '', 'success').then(function() {
                        form.submit();
                    });
                } else if (result.isDenied) {
                    Swal.fire('GAGAL', '', 'info')
                }
            })
        });
    </script>
</body>

</html>