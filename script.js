$(document).ready(function () {
    //hamburger utama
    $(".ham-button").click(function(){
            $(this).toggleClass("fa-xmark");
        $(".subright").toggleClass("actives");
    });
    //hamburger admin
    $(".hams-button").click(function () {
        $(".sidebar").toggleClass("active");
    });
    // slidder
    
    $(".next").click(function () {
        let currentImg = $(".active")
        let nextImg = currentImg.next();
        if(nextImg.length){
            currentImg.removeClass('active').css('z-index',10);
            nextImg.addClass('active').css('z-index', 10);
        } else if(!nextImg.length) {
            $(".first").toggleClass("active");
            currentImg.removeClass('active').css('z-index', 10);
        }
    });

    setInterval(function () {
        $('.next').trigger('click');
    }, 5000);

    $(".prev").click(function () {
        let currentImg = $(".active")
        let prevImg = currentImg.prev();
        if (prevImg.length) {
            currentImg.removeClass('active').css('z-index', 10);
            prevImg.addClass('active').css('z-index', 10)
        } else {
            $(".end").toggleClass("active");
            currentImg.removeClass('active').css('z-index', 10);
        }
    });
    



});

function myFnAdmin(){
    swal.fire({ icon: 'success',
    title: "Anda Berhasil Login!",
    text: "Selamat datang ADMIN, login Success!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "../admin/admin";}
    });
};

function myFnUser(){
    swal.fire({ icon: 'success',
    title: "Anda Berhasil Login!",
    text: "Selamat datang, Selamat Berbelanja!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "katalog";}
    });
};

function myFnGagal(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Username SALAH!',
})
};

function myFnGagalPass(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Password SALAH!',})
};

function tambahOKe() {
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Data Produk Berhasil ditambahkan!",
    type: "success"})
};


function tambahGagal(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Data Produk Gagal ditambahkan!',})
};

function KeranjangDouble(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Barang Sudah Ada di Keranjang!',})
};

function registrasiOke(){
    swal.fire({ icon: 'success',
    title: "Akun Berhasil Dibuat!",
    text: "Silahkan Login",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "login";}
    });
};

function keranjangLogin(){
    Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Harap LOGIN Terlebih dahulu!',
    type: "gagal"}).then(okay => {
    if (okay) {
    window.location.href = "../login";}
    })
};
function ubahOke(){
    swal.fire({ icon: 'success',
    title: "Selamat!",
    text: "Produk Berhasil diubah!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "admin-product";}
    });
};

function pembelianUbah(){
    Swal.fire({
    icon: 'error',
    title: 'Oops...',
    text: 'Harap Isi No.HP & alamat Terlebih dahulu!',
    type: "gagal"}).then(okay => {
    if (okay) {
    window.location.href = "../user/user";}
    })
};

function berhasilBayar(){
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Pesanan Anda sedang kami Proses!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "pesanan.php";}
    });
};
function ubahAkunOke(){
    swal.fire({ icon: 'success',
    title: "Selamat!",
    text: "Data pribadi Berhasil diperbarui!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "user.php";}
    });
};
function ubahAkunGagal(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Data pribadi gagal diperbarui!',})
};

function berhasilBatal(){
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Pesanan Anda dibatalkan!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "pesanan.php";}
    });
};

function buktiGagal(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'Anda Lupa mengupload Bukti Pembayaran!',
    });
};

function usernameGagal(){
    Swal.fire({
  icon: 'error',
  title: 'Oops...',
  text: 'username sudah terdaftar!',
    });
};

function berhasilStatus(){
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Status pesanan berhasil diubah!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "admin-pesanan.php";}
    });
};

function keranjangOke(){
    swal.fire({ icon: 'success',
    title: "Selamat!",
    text: "Produk di keranjang berhasil di hapus",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "../keranjang.php";}
    });
};
function KeranjangGagal(){
    swal.fire({ icon: 'error',
    title: "Opps!",
    text: "Ada erro",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "../keranjang.php";}
    });
};
function berhasilHapus(){
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Product berhasil dihapus!",
    type: "success"}).then(okay => {
    if (okay) {
    window.location.href = "admin-product.php";}
    });
};

function tambahKeranjang() {
    swal.fire({ icon: 'success',
    title: "Berhasil!",
    text: "Produk Berhasil ditambahkan ke Keranjang!",
    type: "success"})
};