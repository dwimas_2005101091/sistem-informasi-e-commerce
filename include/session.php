<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "enryu";
$conn = mysqli_connect($servername, $username, $password, $database);

if (isset($_COOKIE['id']) && isset($_COOKIE['key']) && isset($_COOKIE['level']) && isset($_COOKIE['key_pass']) ) {
    $id  = $_COOKIE['id'];
    $key = $_COOKIE['key'];
    $key_pass = $_COOKIE['key_pass'];
    $level =  $_COOKIE['level'];

    // ambil username berdasarkan id
    $result = mysqli_query($conn, "SELECT * FROM user WHERE id_user = $id");
    $row = mysqli_fetch_assoc($result);

    //cek cokie dan username
    if (mysqli_num_rows($result)) {
        if ($key === hash('sha256', $row['username'])) {
            $_SESSION["login"] = true;
            $_SESSION['level'] = $level;
        }
    }

    //cek cookie id
    if($row["password"]!==$key_pass || $row["level"]!==$level){
        session_unset();
        session_destroy();
        $_SESSION = [];
        $expire = 2 * 30 * 24 * 3600;
        //menghapus cookie
        setcookie('id', '', time() - $expire, '/');
        setcookie('key', '', time() - $expire, '/');
        setcookie('level', '', time() - $expire, '/');
        setcookie('key_pass', '', time() - $expire, '/');
    }
}
