<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "enryu";
$conn = mysqli_connect($servername, $username, $password, $database);

function query($query){
    global $conn;
    $result = mysqli_query($conn, $query);
    $rows = [];
    while ($row = mysqli_fetch_assoc($result)) {
        $rows[] = $row;
    }
    return $rows;
}

function RemoveSpecialChar($str){
    $res = preg_replace('/[\@\&\^\%\(\)\#\$\!\]\[\}\{\*\'\=\"\;\" "]+/', ' ', $str);
    return $res;
}

function registrasi($data){
    global $conn;
    $nama = mysqli_real_escape_string($conn, strtolower(stripslashes($data["nama"])));
    $username = mysqli_real_escape_string($conn, strtolower(stripslashes($data["username"])));
    $email = mysqli_real_escape_string($conn, strtolower(stripslashes($data["email"])));
    $password = mysqli_real_escape_string($conn, $data["password"]);
    //cek username sama
    $result = mysqli_query($conn, "SELECT username FROM user WHERE username ='$username'");

    //cek username sama
    if (mysqli_fetch_assoc($result)) {
        echo '<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>';
        echo '<script src="../script.js"></script>';
        echo "<div><script>usernameGagal()</script></div>";
        return false;
    }

    //cek username apakah string kosong
    if (empty(trim($username))) {
        echo "<script>
        alert('anda memasukkan sting kosong')
        </script>";
        return false;
    }
    //enskripsi password
    $password = password_hash($password, PASSWORD_DEFAULT);

    // var_dump($password);
    mysqli_query($conn, "INSERT INTO user VALUES(NULL,'$nama','$email',NULL,NULL,'$username','$password','USER')");

    return mysqli_affected_rows($conn);
}

function upload($number){
    $error = $_FILES['gambar'.$number.'']['error'];
    $namaFile = $_FILES['gambar' . $number . '']['name'];
    $ukuranFile = $_FILES['gambar' . $number . '']['size'];
    $tmpName = $_FILES['gambar' . $number . '']['tmp_name'];

    ///cek apakah ada gambar yang diupload
    if ($error === 4) {
        echo "<script>
        alert('tambahkan gambar terlebih dahulu')
        </script>";
        return false;
    }
    //cek apakah yang diupload adalah gambar
    $ektensiGambarValid = ['jpg', 'jpeg', 'png'];
    $ektensiGambar = explode('.', $namaFile);
    $ektensiGambar = strtolower(end($ektensiGambar));
    if (!in_array($ektensiGambar, $ektensiGambarValid)) {
        echo "<script>
        alert('yang anda upload bukan gambar')
        </script>";
    }
    // cek ukuran file gambar
    if ($ukuranFile > 3000000) {
        echo "<script>
        alert('ukuran gambar terlalu besar')
        </script>";
        return false;
    }

    //gambar lolos cek kualiti

    ///generate nama gambar baru
    $namaFileBaru = uniqid();
    $namaFileBaru .= ".";
    $namaFileBaru .= $ektensiGambar;

    move_uploaded_file($tmpName, '../img/' . $namaFileBaru);

    return $namaFileBaru;
}

function tambah($data){
    global $conn;
    $nama = $data["nama"];
    $harga = $data["harga"];
    $stok = $data["stok"];
    $desc_short = $data["desc_short"];
    $desc_long = $data["desc_long"];
    
    $gambar1 = upload("1");
    $gambar2 = upload("2");
    $gambar3 = upload("3");
    $gambar4 = upload("4");
    if (!$gambar1 || !$gambar2 || !$gambar3) {
        return false;
    }
    //query insert data
    $query = "INSERT INTO produk VALUES(NULL,'$nama','$harga','$stok','$desc_short','$desc_long','$gambar1','$gambar2','$gambar3','$gambar4')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function keranjang($produkId,$userId){
    global $conn;

    
    $result = mysqli_query($conn, "SELECT id_produk FROM keranjang WHERE id_produk =$produkId and id_user=$userId");


    if (mysqli_num_rows($result)) {
        return 3;
    }

    mysqli_query($conn, "INSERT INTO keranjang VALUES(NULL,'$produkId','$userId')");

    return mysqli_affected_rows($conn);
}
function keranjangHapus($produkId, $userId){
    global $conn;

    mysqli_query($conn, "DELETE FROM keranjang WHERE id_produk ='$produkId' AND id_user='$userId' ");

    return mysqli_affected_rows($conn);
}
function cari($keyword){
    global $conn;
    $keyword = mysqli_real_escape_string($conn, $keyword);
    $query = "SELECT * FROM produk WHERE nama_barang LIKE '%$keyword%' ";
    return query($query);
}


function cariKeranjang($keyword,$id){
    global $conn;
    $keyword = mysqli_real_escape_string($conn, $keyword);
    $query = "SELECT id_keranjang, id_user, produk.id_produk, nama_barang, harga, gambar1
FROM keranjang INNER JOIN produk ON keranjang.id_produk = produk.id_produk WHERE id_user = '$id' AND nama_barang LIKE '%$keyword%' ";
    return query($query);
}
function pengaturan($data,$id){
    global $conn;
    $nama = $data['nama'];
    $alamat = $data["alamat"];
    $hp = $data["no_hp"];

    //query insert data
    $query = "UPDATE user SET nama_lengkap = '$nama', alamat = '$alamat', no_handphone = '$hp' WHERE id_user = $id;";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function beliProduk($data, $userId){
    global $conn;
    $jumlah_beli = $data['jumlah'];
    $ukuran = $data['ukuran'];
    $id_produk = $data['id_produk'];
    $result = mysqli_query($conn, "SELECT * FROM produk WHERE id_produk = $id_produk");
    $row = mysqli_fetch_assoc($result);
    $harga= $row['harga'];
    $total=$jumlah_beli*$harga;
    $query = "INSERT INTO transaksi(id_transaksi, id_user, id_produk, ukuran, jumlah_pembelian, total_bayar, info_status) 
    VALUES (NULL, '$userId', '$id_produk', '$ukuran', '$jumlah_beli', '$total','BELUM BAYAR')";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function ubah($data){
    global $conn;
    $id = $data["id"];
    $nama = $data["nama"];
    $harga = $data["harga"];
    $stok = $data["stok"];
    $desc_short = $data["desc_short"];
    $desc_long = $data["desc_long"];
    $gambarLama1 = ($data["gambarLama1"]);
    $gambarLama2 = ($data["gambarLama2"]);
    $gambarLama3 = ($data["gambarLama3"]);
    $gambarLama4 = ($data["gambarLama4"]);
    //cek apakah user upload gambar baru atau tidak
    if ($_FILES['gambar1']['error'] == 4) {
        $gambar1 = $gambarLama1;
    } else {
        $gambar1 = upload("1");
    }
    if ($_FILES['gambar2']['error'] == 4) {
        $gambar2 = $gambarLama2;
    } else {
        $gambar2 = upload("2");
    }
    if ($_FILES['gambar3']['error'] == 4) {
        $gambar3 = $gambarLama3;
    } else {
        $gambar3 = upload("3");
    }
    if ($_FILES['gambar4']['error'] == 4) {
        $gambar4 = $gambarLama4;
    } else {
        $gambar4 = upload("4");
    }

    $query = "UPDATE produk SET
                nama_barang = '$nama', 
                harga ='$harga',
                stok = '$stok',
                desc_short = '$desc_short',
                desc_long = '$desc_long',
                gambar1 = '$gambar1',
                gambar2 = '$gambar2',
                gambar3 = '$gambar3',
                gambar4 = '$gambar4' WHERE id_produk=$id";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function batalTransaksi($id_transaksi,$id_user){
    global $conn;
    $query= "DELETE FROM transaksi WHERE id_transaksi='$id_transaksi' AND id_user= '$id_user'";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
function uploadBukti(){
    $namaFile = $_FILES['gambar']['name'];
    $ukuranFile = $_FILES['gambar']['size'];
    $error = $_FILES['gambar']['error'];
    $tmpName = $_FILES['gambar']['tmp_name'];

    ///cek apakah ada gambar yang diupload
    if ($error === 4) {
        echo '<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>';
        echo '<script src="../script.js"></script>';
        echo "<div><script>buktiGagal()</script></div>";
        return false;
    }

    //cek apakah yang diupload adalah gambar
    $ektensiGambarValid = ['jpg', 'jpeg', 'png'];
    $ektensiGambar = explode('.', $namaFile);
    $ektensiGambar = strtolower(end($ektensiGambar));
    if (!in_array($ektensiGambar, $ektensiGambarValid)) {
        echo "<script>
        alert('yang anda upload bukan gambar')
        </script>";
    }
    // cek ukuran file gambar
    if ($ukuranFile > 3000000) {
        echo "<script>
        alert('ukuran gambar terlalu besar')
        </script>";
        return false;
    }

    //gambar lolos cek kualiti

    ///generate nama gambar baru
    $namaFileBaru = uniqid();
    $namaFileBaru .= ".";
    $namaFileBaru .= $ektensiGambar;

    move_uploaded_file($tmpName, '../img/img-transfer/' . $namaFileBaru);

    return $namaFileBaru;
}
function KirimBukti($transaksi_id, $userId){
    global $conn;
    $gambar = uploadBukti();
    if($gambar==false){
        return 0;
        exit;
    }else {
        $query = "UPDATE transaksi SET bukti_transfer='$gambar', info_status ='LUNAS' WHERE id_transaksi='$transaksi_id' AND id_user='$userId'";
        mysqli_query($conn, $query);
        return mysqli_affected_rows($conn);
    }
}

function adminTransaksi($data){
    global $conn;
    $id = $data['id'];
    $keterangan = $data['keterangan'];
    $status = $data['status'];
    $query = "UPDATE transaksi SET keterangan='$keterangan', info_status ='$status' WHERE id_transaksi='$id'";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}

function terimaProduk($data, $userId){
    global $conn;
    $id = $data['id'];
    $query = "UPDATE transaksi SET info_status ='DI TERIMA' WHERE id_transaksi='$id' AND id_user='$userId'";
    mysqli_query($conn, $query);
    return mysqli_affected_rows($conn);
}
function rupiah($angka){

    $hasil_rupiah = "Rp." . number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

function hapus($id){
    global $conn;
    mysqli_query($conn, "DELETE FROM keranjang WHERE id_produk = '$id' ");
    mysqli_query($conn, "DELETE FROM transaksi WHERE id_produk = '$id' ");
    mysqli_query($conn, "SET FOREIGN_KEY_CHECKS=0");
    mysqli_query($conn, "DELETE FROM produk WHERE id_produk = '$id' ");
    mysqli_query($conn, "SET FOREIGN_KEY_CHECKS=1");
    return mysqli_affected_rows($conn);
}