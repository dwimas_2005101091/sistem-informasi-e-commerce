<?php
session_start();
include "include/functions.php";
include "include/session.php";


$produk = query("SELECT * FROM produk ORDER BY id_produk DESC LIMIT 12");
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Dwimas Budi Sulistyo">
	<meta name="description" content="Website untuk membeli sepatu-sepatu keren dengan harga terjangkau">
	<title>Enryu</title>
	<link rel="stylesheet" href="style/style.css">
	<link rel="stylesheet" href="style/slide-style.css">
	<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
	<link rel="icon" type="image/x-icon" href="assets/favicon.ico">
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<script src="script.js"></script>
</head>

<body>
	<!-- BAGIAN NAVIGASI ATAS -->
	<nav>
		<div class="logo-center">
			<div class="fav">
				<a href="./">Enryu<span>.</span>co</a>
			</div>
			<div class="ham">
				<i class="ham-button fa-solid fa-bars fades"></i>
			</div>
		</div>

		<div class="navsub subright fades">
			<ul>
				<li>
					<div class="searchBar">
						<form action="menu/katalog" method="GET">
							<input type="text" id="search" name="keyword" value="<?php if (isset($_GET['keyword'])) {
																						echo $_GET['keyword'];
																					} ?>">
							<button>
								<i class="fa-solid fa-magnifying-glass"></i>
							</button>
						</form>
					</div>
				</li>
				<li><a href="menu/katalog">KATALOG</a></li>
				<?php if (isset($_SESSION["login"])) { ?>
					<?php if ($_SESSION["level"] === "SU_ADMIN") { ?>
						<li><a href="admin/admin">ADMIN DASHBOARD</a></li>
					<?php } else { ?>
						<li><a href="menu/keranjang">KERANJANG</a></li>
						<li><a href="menu/pesanan">PESANAN</a></li>
						<li><a href="user/user">AKUN</a></li>
					<?php } ?>
					<li><a href="menu/logout.php">LOG OUT</a></li>
				<?php  } else if (!isset($_SESSION["login"])) { ?>
					<li><a href="menu/login">SIGN IN</a></li>
					<li><a href="menu/sign-up">SIGN UP</a></li>
				<?php  } ?>
			</ul>
		</div>
	</nav>
	<div class="spacer"></div>
	<!-- AKHIR DARI BAGIAN NAVIGASI ATAS -->
	<!-- Main/Bagian Utama -->

	<div class="slider-outer homepage">
		<i class="fa-solid  fa-chevron-left prev homepage"></i>
		<div class="slidder-inner homepage">
			<div class="main-headline">
				<div class="shoes-img" data-aos="fade-left">
					<img src="img/sample-main8.png" alt="sepatu-keren" title="sepatu keren" class="active fades first">
					<img src="img/sample-main12.png" alt="sepatu-keren" title="sepatu keren" class="fades">
					<img src="img/sample-main10.png" alt="sepatu-keren" title="sepatu keren" class="fades">
					<img src="img/sample-main9.png" alt="sepatu-keren" title="sepatu keren" class="end fades">
				</div>
				<div class="desc fade" data-aos="fade-right">
					<h3>MAKE YOUR <br>MOVE.<br> UP TO 50% OFF</h3>
					<P>We only do this 2x a year and you can't miss it. No code needed Select Styles. Prices in red. Ends 7/6</P>
					<a href="menu/produk/5010"><button>SHOP NOW</button></a>
				</div>
			</div>
		</div>
		<i class="fa-solid fa-chevron-right next homepage"></i>
	</div>
	<!-- stutu -->
	<div class="headline">
		<h2>Product List</h2>
	</div>
	<main>
		<?php foreach ($produk as $row) : ?>
			<a href="menu/produk/<?= $row["id_produk"] ?>" data-aos="zoom-in-up">
				<section>
					<div class="product-img">
						<img src="img/<?= $row["gambar1"] ?>" alt="sepatu keren">
					</div>
					<div class="caption">
						<h5><?= $row["nama_barang"]; ?></h5>
						<p><?= rupiah($row["harga"]); ?></p>
					</div>
				</section>
			</a>
		<?php endforeach; ?>
	</main>
	<!-- <div class="spacer"></div> -->
	<!-- footer -->
	<footer>
		<div class="footer-botom">
			<div class="foot foot-left">
				<p>© 2022 ENRYU CO</p>
			</div>
			<div class="foot foot-center">
				<a href="#">ABOUT</a>
				<a href="#">TERMS AND CONDITIONS</a>
				<a href="#">PRIVACY</a>
			</div>
			<div class="foot foot-right">
				<p>Created by Dwimas Budi Sulistyo</p>
			</div>
		</div>
	</footer>
	<script>
		AOS.init({
			once: true,
		});
	</script>
</body>

</html>