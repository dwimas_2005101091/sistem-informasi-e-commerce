-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 04, 2023 at 02:41 PM
-- Server version: 8.0.30
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `enryu`
--
CREATE DATABASE IF NOT EXISTS `enryu` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `enryu`;
-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE `keranjang` (
  `id_keranjang` int NOT NULL,
  `id_produk` int DEFAULT NULL,
  `id_user` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `keranjang`
--

INSERT INTO `keranjang` (`id_keranjang`, `id_produk`, `id_user`) VALUES
(37, 5010, 1019),
(38, 5006, 1019),
(39, 5002, 1019),
(44, 5008, 1021),
(45, 5008, 1023),
(52, 5010, 1024),
(54, 5003, 1024),
(55, 5007, 1005),
(56, 5006, 1005),
(58, 5016, 1003),
(61, 5007, 1025);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int NOT NULL,
  `nama_barang` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `harga` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `stok` int DEFAULT NULL,
  `desc_short` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `desc_long` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gambar1` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `gambar2` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gambar3` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `gambar4` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama_barang`, `harga`, `stok`, `desc_short`, `desc_long`, `gambar1`, `gambar2`, `gambar3`, `gambar4`) VALUES
(5001, 'Nike Air Force 1 Low Retro', '500000', 99, 'Sepatu keren buatan nike', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main.png', 'sample-main2.png', 'sample-main2.png', 'sample-main4.png'),
(5002, 'Nike Free RN Flyknit 3', '700000', 99, 'Sepatu keren nih bos', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main4.png', 'sample-main5.png', 'sample-main2.png', 'sample-main.png'),
(5003, 'Nike Air Huarache Run DNA', '900000', 99, 'Sepatu keren lagi', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main2.png', 'sample-main7.png', 'sample-main8.png', 'sample-main9.png'),
(5004, 'Nike Air Max 270 React RS', '850000', 46, 'Sepatu Mahal NIh, Senggol Dong', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main7.png', 'sample-main8.png', 'sample-main14.png', 'sample-main12.png'),
(5005, 'Nike Classic Cortez', '550000', 99, 'sepatu keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main5.png', 'sample-main.png', 'sample-main6.png', 'sample-main2.png'),
(5006, 'Nike Air VaporMax 2019', '750000', 99, 'Sepatu Keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main13.png', 'sample-main.png', 'sample-main2.png', 'sample-main8.png'),
(5007, 'Nike Air Max 95', '3500000', 99, 'Sepatu Keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main8.png', 'sample-main2.png', 'sample-main14.png', 'sample-main3.png'),
(5008, 'Nike Air Zoom Alphafly Next', '2500000', 99, 'Sepatu Keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main9.png', 'sample-main12.png', 'sample-main.png', 'sample-main2.png'),
(5009, 'Nike Air Jordan XXXV DNA', '950000', 99, 'Sepatu Keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main10.png', 'sample-main7.png', 'sample-main6.png', 'sample-main6.png'),
(5010, 'Nike Roshe Two Flyknit', '8500000', 99, 'Sepatu Keren', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main11.png', 'sample-main13.png', 'sample-main2.png', 'sample-main5.png'),
(5011, 'Nike Court Royale 2022', '550000', 56, 'Sepatu Keren senggol dong', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quas repudiandae ipsum maxime sed perferendis eos, odio debitis. Nam iusto, perferendis doloremque sunt aliquam tempore molestias laudantium saepe, quam, reprehenderit cupiditate.', 'sample-main4.png', 'sample-main2.png', 'sample-main4.png', 'sample-main7.png'),
(5016, 'Nike Loro Ati iki 2022', '552000', 10, 'Sepatu Nike Loro ati ninggal janji ', 'Lorem ipsum dolor amet', 'sample-main12.png', 'sample-main14.png', 'sample-main3.png', 'sample-main2.png');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int NOT NULL,
  `id_user` int NOT NULL,
  `id_produk` int NOT NULL,
  `ukuran` int NOT NULL,
  `jumlah_pembelian` int NOT NULL,
  `total_bayar` int NOT NULL,
  `keterangan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bukti_transfer` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `info_status` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_user`, `id_produk`, `ukuran`, `jumlah_pembelian`, `total_bayar`, `keterangan`, `bukti_transfer`, `info_status`) VALUES
(90000043, 1019, 5011, 37, 1, 1500000, 'Kamu penipu', 'transfer1.jpg', 'DI TERIMA'),
(90000045, 1019, 5011, 34, 1, 1500000, '', 'transfer1.jpg', 'LUNAS'),
(90000046, 1006, 5001, 39, 12, 6000000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000047, 1003, 5010, 36, 12, 102000000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000050, 1020, 5003, 37, 1, 900000, 'HAHAHA', 'transfer1.jpg', 'DI KEMAS'),
(90000056, 1003, 5011, 35, 12, 18000000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000057, 1005, 5008, 42, 1, 2500000, 'HAHAHA', 'transfer1.jpg', 'DI TERIMA'),
(90000059, 1005, 5004, 36, 12, 10200000, 'Resi JNT 9900000121', 'transfer1.jpg', 'DI KIRIM'),
(90000060, 1005, 5013, 42, 1, 550000, 'NO RESI 2123123123', 'transfer1.jpg', 'DI KIRIM'),
(90000061, 1019, 5011, 36, 12, 18000000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000062, 1019, 5007, 36, 1, 3500000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000063, 1019, 5008, 41, 10, 25000000, 'Resi  JNT 1233242342', 'transfer1.jpg', 'DI KEMAS'),
(90000067, 1019, 5001, 36, 1, 500000, 'PENIPU', 'transfer1.jpg', 'LUNAS'),
(90000069, 1019, 5007, 36, 1, 3500000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000071, 1019, 5010, 37, 1, 8500000, 'OKE BANG', 'transfer1.jpg', 'DI KEMAS'),
(90000074, 1021, 5008, 42, 9, 22500000, 'RESI JNT 982423489234', 'transfer1.jpg', 'LUNAS'),
(90000076, 1023, 5008, 40, 2, 5000000, 'Resi JNT 743759784384', 'transfer1.jpg', 'LUNAS'),
(90000079, 1003, 5001, 42, 1, 500000, 'GIVE AWAY', 'transfer1.jpg', 'LUNAS'),
(90000081, 1024, 5010, 42, 2, 17000000, 'NO RESI JNT 45454343421', 'transfer1.jpg', 'DI TERIMA'),
(90000083, 1024, 5009, 42, 2, 19000000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000084, 1024, 5016, 42, 2, 1104000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000085, 1024, 5011, 42, 3, 1650000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000086, 1024, 5010, 42, 3, 25500000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000087, 1024, 5009, 42, 1, 950000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000088, 1024, 5008, 41, 1, 2500000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000089, 1024, 5007, 41, 5, 17500000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000090, 1024, 5006, 42, 1, 750000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000091, 1024, 5005, 41, 4, 2200000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000092, 1024, 5004, 42, 2, 1700000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000093, 1024, 5003, 42, 2, 1800000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000094, 1024, 5002, 40, 2, 1400000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000095, 1024, 5001, 41, 2, 1000000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000096, 1005, 5016, 42, 1, 552000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000097, 1005, 5011, 41, 1, 550000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000098, 1005, 5010, 42, 2, 17000000, NULL, 'transfer1.jpg', 'BELUM BAYAR'),
(90000099, 1005, 5009, 41, 2, 1900000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000100, 1005, 5008, 41, 1, 2500000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000101, 1005, 5007, 41, 2, 7000000, '', 'transfer1.jpg', 'DI KIRIM'),
(90000102, 1005, 5006, 41, 2, 1500000, '', 'transfer1.jpg', 'DI TERIMA'),
(90000103, 1005, 5005, 42, 2, 1100000, '', 'transfer1.jpg', 'DI TERIMA'),
(90000104, 1005, 5004, 42, 7, 5950000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000105, 1020, 5016, 42, 1, 552000, '', 'transfer1.jpg', 'DI TERIMA'),
(90000106, 1020, 5011, 42, 1, 550000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000107, 1020, 5010, 42, 1, 8500000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000108, 1020, 5009, 41, 1, 950000, '', 'transfer1.jpg', 'DI KEMAS'),
(90000109, 1020, 5008, 42, 1, 2500000, '', 'transfer1.jpg', 'LUNAS'),
(90000110, 1020, 5007, 41, 1, 3500000, '', 'transfer1.jpg', 'LUNAS'),
(90000111, 1020, 5006, 42, 2, 1500000, '', 'transfer1.jpg', 'LUNAS'),
(90000112, 1020, 5005, 42, 5, 2750000, '', 'transfer1.jpg', 'LUNAS'),
(90000113, 1020, 5005, 42, 1, 550000, '', 'transfer1.jpg', 'LUNAS'),
(90000114, 1020, 5004, 42, 1, 850000, '', 'transfer1.jpg', 'LUNAS'),
(90000115, 1020, 5003, 38, 1, 900000, '', 'transfer1.jpg', 'LUNAS'),
(90000116, 1023, 5009, 41, 12, 11400000, NULL, NULL, 'BELUM BAYAR'),
(90000117, 1023, 5006, 41, 1, 750000, NULL, NULL, 'BELUM BAYAR'),
(90000118, 1019, 5006, 37, 12, 9000000, NULL, NULL, 'BELUM BAYAR'),
(90000119, 1019, 5001, 34, 12, 6000000, NULL, NULL, 'BELUM BAYAR'),
(90000120, 1005, 5005, 38, 1, 550000, NULL, NULL, 'BELUM BAYAR'),
(90000121, 1025, 5006, 42, 5, 3750000, 'RESI JNT 1412412412', 'transfer1.jpg', 'DI TERIMA');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int NOT NULL,
  `nama_lengkap` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `alamat` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `no_handphone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `password` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `level` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama_lengkap`, `email`, `alamat`, `no_handphone`, `username`, `password`, `level`) VALUES
(1000, 'SUPER_USER', 'SUPER_USER', NULL, NULL, 'root', '$2y$10$Kj7RPGj2AYqPHdWUKMCSZulnvimvcsrILE4V4wUSuQYoAyE2tnxu2', 'SU_ADMIN'),
(1001, 'SUPER_USER', 'SUPER_USER', NULL, NULL, 'admin', '$2y$10$Kj7RPGj2AYqPHdWUKMCSZulnvimvcsrILE4V4wUSuQYoAyE2tnxu2', 'SU_ADMIN'),
(1002, 'Dwimas Budi Sulstyo', 'dwimas@budi.com', NULL, NULL, 'dwimas', '$2y$10$Bg8tEqTm/IOG8CLLHd8pDeZJX21u7mHZJXfurQnD261eTs0Zrcf/', 'USER'),
(1003, 'Dwimas Budi Sulistyo', 'user@gmail.com', 'ngawi', '081234567890', 'user', '$2y$10$PYuMv4Q/RKsOoX.aIX25.uAL/S/UvrvaqiouB7ssqq52cTsiQTMu2', 'USER'),
(1004, 'Alexander', 'alex@gmail.com', NULL, NULL, 'alex', '$2y$10$U0Oao3gLfK3N80xIFKYRIeKTkWXvsMOHWsXuwjA86SYj5eBbi0J1y', 'USER'),
(1005, 'messi', 'messi@gmail.com', 'PARIS', '098123456789', 'messi', '$2y$10$F551YFCwDfW6kknQAPBWGeCqIjE33173lNCv0/c0jHyvxqzI74JT.', 'USER'),
(1006, 'kipli', 'kipli@gmail.com', 'RT.03 rw 0.9 dsn pulorejo kec kedungggalar kab ngawi', '0812345678', 'kipli', '$2y$10$t/zRyt/XMUlYE5ORN9i4IOfV86qlw..5tOgNP2vbvvxblkbkiEURi', 'USER'),
(1009, 'ewewr', 'werw@sad', NULL, NULL, 'ewrwerwer', '$2y$10$ePlpkBc6fxm4XyVo7cxo4e/BLKv9edVt3XA52K68vHN9Bf2r12icC', 'USER'),
(1010, 'asmr', 'asmr', NULL, NULL, 'asmr', 'asmr', 'SU_ADMIN'),
(1018, 'sulis', 'sulis@gmail.com', NULL, NULL, 'sulis', '$2y$10$2L61pSTdaf3M3GdiMpA6heuL8rTeH0gGa5okGssmarIZXJiv/oX0y', 'USER'),
(1019, 'BEJO SI PALING BEJO', 'bejo@gmail.com', 'KEDUNGGALAR NGAWI 63254 JAWI TIMUR INDONESIA', '089696574935', 'bejo', '$2y$10$8x2UNEhEbVOqj4EGMIZn3uyQPbdSCcuPE0N7JE7oxXH7YW.Nrzj7C', 'USER'),
(1020, 'saipul jamil', 'saipul@gmail.com', 'JL SUHADA', '081234567890', 'saipul', '$2y$10$se9faaOVVOyw6kZKHCPipOB/xu7pgNvE1tbn5L8S.nH/px9xxQolS', 'USER'),
(1021, 'farhan suka rehan', 'farhan21@gmail.com', 'KANIGORO MADIUN JAWA TIMUR INDONESIA ', '08123456789', 'farhan', '$2y$10$qJMLe4eROLL0j2qD6QUwK.dblIl59Io7nl1ejG/E9LUKtRBg.qdHC', 'USER'),
(1022, 'wisanggeni', 'wisanggeni@gmail.com', NULL, NULL, 'wisanggeni', '$2y$10$HFZYO8jnq.qYrHEGfBXjdu.GiVR42o4oDKrBuodjnjZv3igKvoWeC', 'USER'),
(1023, 'Saiful Bin Ipul', 'saiful@gmail.com', 'KANIGORO MADIUN JAWA TIMUR INDONESIA', '089696574930', 'saiful', '$2y$10$YLoZ.NhoLAmSk4U0NR48AOULju/ZKRXvUK2DB4CMur4eMi/1XAyEC', 'USER'),
(1024, 'faisal bin usman', 'faisal@gmail.com', 'Ngawi', '08123456789', 'faisal', '$2y$10$Fl.g41tQahCa.o7r561eZeK1dpkwGqzantbG0lQbf/TTUGCuGO7eG', 'USER'),
(1025, 'supri', 'supri@gmail.com', 'Ngawi', '08123456789', 'supri', '$2y$10$DNCKgS97IGjAfA3Mdc7OseEp6ksIsmtsv7kPdTguGn1Hw/ST/1zCm', 'USER');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD PRIMARY KEY (`id_keranjang`),
  ADD KEY `produkId_idx` (`id_produk`),
  ADD KEY `userId_idx` (`id_user`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_produk_idx` (`id_produk`),
  ADD KEY `id_user_idx` (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keranjang`
--
ALTER TABLE `keranjang`
  MODIFY `id_keranjang` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5017;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90000122;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1026;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `produkId` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `userId` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `id_produk` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id_produk`),
  ADD CONSTRAINT `id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
