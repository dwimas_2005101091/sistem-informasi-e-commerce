<?php
//session
include "../include/functions.php";
include "../include/session.php";

session_start();
if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}
//sesion

$produk = query("SELECT * FROM produk ORDER BY id_produk DESC");

if (isset($_GET['keyword'])) {
    $keyword2 = $_GET['keyword'];
    //pagenation
    $jumDataPerhalaman = 10;
    $jumlahData = count(query("SELECT * FROM produk
WHERE nama_barang LIKE '%$keyword2%' OR desc_short LIKE '%$keyword2%' OR harga LIKE '%$keyword2%'"));
    $jumlahHalaman = ceil($jumlahData / $jumDataPerhalaman);
    if (isset($_GET['halaman'])) {
        $halamanAktif = $_GET['halaman'];
    } else {
        $halamanAktif = 1;
    }
    $awalData = ($jumDataPerhalaman * $halamanAktif) - $jumDataPerhalaman;
    $produk = query("SELECT * FROM produk
WHERE nama_barang LIKE '%$keyword2%' OR desc_short LIKE '%$keyword2%' OR harga LIKE '%$keyword2%' ORDER BY id_produk DESC LIMIT $awalData,$jumDataPerhalaman");
}

if (!isset($_GET['keyword'])) {
    //pagination
    $keyword2 = '';
    $jumDataPerhalaman = 10;
    $jumlahData = count(query("SELECT * FROM produk
WHERE nama_barang LIKE '%$keyword2%' OR desc_short LIKE '%$keyword2%' OR harga LIKE '%$keyword2%'
ORDER BY id_produk DESC"));
    $jumlahHalaman = ceil($jumlahData / $jumDataPerhalaman);
    if (isset($_GET['halaman'])) {
        $halamanAktif = $_GET['halaman'];
    } else {
        $halamanAktif = 1;
    }
    //cara ternary
    // $halamanAktif = (isset($_GET['halaman'])) ? $_GET['halaman'] : 1;
    $awalData = ($jumDataPerhalaman * $halamanAktif) - $jumDataPerhalaman;
    $produk = query("SELECT * FROM produk
WHERE nama_barang LIKE '%$keyword2%' OR desc_short LIKE '%$keyword2%' OR harga LIKE '%$keyword2%'
ORDER BY id_produk DESC LIMIT $awalData, $jumDataPerhalaman");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <title>Admin dashboard</title>
    <link rel="stylesheet" href="../style/admin-style.css">
    <link rel="stylesheet" href="style-admin/product.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>

<body>
    <div class="container">
        <div class="sidebar">
            <div class="head-icon">
                <a href="">Enryu Admin</a>
            </div>
            <div class="content">
                <a class="list-item" href="admin.php">
                    <i class="fa-solid fa-house-chimney"></i>
                    <p class="item-info hidden">HOME</p>
                </a>
                <a class="list-item" href="../">
                    <i class="fa-solid fa-globe"></i>
                    <p class="item-info hidden">Website</p>
                </a>
                <a class="list-item " href="admin-tambah-product.php">
                    <i class="fa-solid fa-star"></i>
                    <p class"item-info">TAMBAH ITEM</p>
                </a>
                <a class="list-item active" href="admin-product.php">
                    <i class="fa-solid fa-store"></i>
                    <p class"item-info">PRODUCT</p>
                </a>
                <a class="list-item" href="admin-pesanan.php">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p class"item-info">PESANAN</p>
                </a>
                <a class="list-item " href="admin-lunas.php">
                    <i class="fa-solid fa-coins"></i>
                    <p class"item-info">LUNAS</p>
                </a>
                <a class="list-item " href="admin-kemas.php">
                    <i class="fa-solid fa-boxes-packing"></i>
                    <p class"item-info">DI KEMAS</p>
                </a>
                <a class="list-item" href="admin-kirim.php">
                    <i class="fa-solid fa-truck-fast"></i>
                    <p class"item-info">DIKIRIM</p>
                </a>
            </div>
        </div>
        <div class="main">
            <div class="main-header">
                <div class="hams">
                    <i class="hams-button fa-solid fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="">Admin Dashboard</a>
                </div>
                <div class="account">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name">Dwimas</p>
                        <p class="acc-role">Super Admin</p>
                    </div>
                </div>
            </div>
            <div class="main-content main--product">
                <form action="" method="GET">
                    <input type="text" name="keyword" id="search" autofocus placeholder="masukkan keyword pencarian" autocomplete="off">
                    <button type="submit" class="search">Cari</button>
                </form>
                <h1>Daftar Product</h1>
                <div id="container">
                    <table border="1" cellpadding="10" cellspacing="0">
                        <tr class="t-head">
                            <th>No.</th>
                            <th>Aksi</th>
                            <th>Gambar</th>
                            <th>Nama</th>
                            <th>Harga</th>
                            <th>Stok</th>
                            <th>Deskripsi singkat</th>
                        </tr>
                        <?php
                        if (isset($_GET['halaman'])) {
                            if ($_GET['halaman'] == 1) {
                                $j = 1;
                            } else {
                                $j = $_GET['halaman'] * 10 - 9;
                            }
                        } else {
                            $j = 1;
                        }
                        ?>
                        <?php $i = $j ?>
                        <?php foreach ($produk as $row) : ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td class="option">
                                    <a href="admin-ubah.php?id=<?= $row["id_produk"] ?>">Edit</a>
                                    <a href="hapus.php?id=<?= $row["id_produk"] ?>" id="hapus">Hapus</a>
                                </td>
                                <td>
                                    <img src="../img/<?= $row["gambar1"] ?>" alt="gambar">
                                </td>
                                <td>
                                    <p><?= $row["nama_barang"]; ?></p>
                                </td>
                                <td>
                                    <p><?= rupiah($row["harga"]); ?></p>
                                </td>
                                <td>
                                    <p><?= $row["stok"]; ?></p>
                                </td>
                                <td>
                                    <p><?= $row["desc_short"]; ?></p>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div class="page">
                    <div class="page">
                        <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>
                            <?php if ($i == $halamanAktif) : ?>
                                <a href="?halaman=<?= $i; ?><?php if (isset($_GET['keyword'])) {
                                                                echo '&keyword=' . $keyword2;
                                                            }; ?>" style="font-weight:bold; color:red;">
                                    <?= $i; ?>
                                </a>
                            <?php else : ?>
                                <a href="?halaman=<?= $i; ?><?php if (isset($_GET['keyword'])) {
                                                                echo '&keyword=' . $keyword2;
                                                            }; ?>">
                                    <?= $i; ?>
                                </a>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        document.querySelector('#hapus').addEventListener('click', function(e) {
            e.preventDefault(); // <--- prevent the link from being followed

            var url = this.getAttribute("href"); // get the value of the "href" attribute of the link

            swal.fire({
                title: 'Kamu yakin?',
                text: "Ingin menghapus produk ini!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Oke',
                dangerMode: true,
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    window.location.href = url; // redirect the user to the URL
                } else if (result.isDenied) {
                    Swal.fire('Changes are not saved', '', 'info')
                }
            })
        });
    </script>
</body>

</html>