<?php
//session
session_start();

if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}

//sesion

include "../include/functions.php";

$id = $_GET["id"];

if(hapus($id)>=0){
    echo '<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>';
    echo '<script src="../script.js"></script>';
    echo "<div><script>berhasilHapus()</script></div>";
} else {
    echo '<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>';
    echo '<script src="../script.js"></script>';
    echo "<div><script>berhasilHapus()</script></div>";
}

?>