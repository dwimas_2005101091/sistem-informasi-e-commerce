<?php
//session
include "../include/functions.php";
include "../include/session.php";

session_start();
if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}
$transaksi_id =  RemoveSpecialChar((int)$_GET['id']);

$transaksi = query("SELECT id_transaksi, id_user, transaksi.id_produk, nama_barang, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status, keterangan, bukti_transfer
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk WHERE transaksi.id_transaksi ='$transaksi_id'")[0];

$id_user = $transaksi['id_user'];
$user = query("SELECT * FROM user where id_user='$id_user'")[0];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <title>Admin dashboard</title>
    <link rel="stylesheet" href="../style/admin-style.css">
    <link rel="stylesheet" href="style-admin/product.css">
    <link rel="stylesheet" href="style-admin/detail-pesanan.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>
<?php
if (isset($_POST['simpan'])) {
    if (adminTransaksi($_POST) >= 0) {
        echo '<div><script>';
        echo 'berhasilStatus();';
        echo '</script></div>';
    }
    // var_dump($_POST);
}

?>

<body>
    <div class="container">
        <div class="sidebar">
            <div class="head-icon">
                <a href="">Enryu Admin</a>
            </div>
            <div class="content">
                <a class="list-item" href="admin.php">
                    <i class="fa-solid fa-house-chimney"></i>
                    <p class="item-info hidden">HOME</p>
                </a>
                <a class="list-item" href="../">
                    <i class="fa-solid fa-globe"></i>
                    <p class="item-info hidden">Website</p>
                </a>
                <a class="list-item " href="admin-tambah-product.php">
                    <i class="fa-solid fa-star"></i>
                    <p class"item-info">TAMBAH ITEM</p>
                </a>
                <a class="list-item" href="admin-product.php">
                    <i class="fa-solid fa-store"></i>
                    <p class"item-info">PRODUCT</p>
                </a>
                <a class="list-item" href="admin-pesanan.php">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p class"item-info">PESANAN</p>
                </a>
                <a class="list-item " href="admin-lunas.php">
                    <i class="fa-solid fa-coins"></i>
                    <p class"item-info">LUNAS</p>
                </a>
                <a class="list-item " href="admin-kemas.php">
                    <i class="fa-solid fa-boxes-packing"></i>
                    <p class"item-info">DI KEMAS</p>
                </a>
                <a class="list-item" href="admin-kirim.php">
                    <i class="fa-solid fa-truck-fast"></i>
                    <p class"item-info">DIKIRIM</p>
                </a>
            </div>
        </div>
        <div class="main">
            <div class="main-header">
                <div class="hams">
                    <i class="hams-button fa-solid fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="">Admin Dashboard</a>
                </div>
                <div class="account">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name">Dwimas</p>
                        <p class="acc-role">Super Admin</p>
                    </div>
                </div>
            </div>
            <div class="main-content main--product">
                <!-- konten utama-->
                <div class="wrapper-pembelian">
                    <div class="pembelian-detail">
                        <div class="container-admin-pemesanan">
                            <div class="invoice">
                                <h2>INVOICE#<?= $transaksi['id_transaksi'] ?></h2>
                                <p class="red">STATUS : <?= $transaksi['info_status'] ?></p>
                                <div class="info-rekening">
                                    <img src="../img/img-transfer/<?= $transaksi['bukti_transfer'] ?>" alt="">
                                </div>
                                <div class="user-desc">
                                    <div class="li-user">
                                        <p>Nama : <?= $user['nama_lengkap'] ?></p>
                                    </div>
                                    <div class="li-user">
                                        <p>No Handphone : <?= $user['no_handphone'] ?></p>
                                    </div>
                                    <div class="li-alamat">
                                        <p>Alamat : <?= $user['alamat'] ?></p>
                                        <p></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="invoice-desc">
                                    <div class="hr">
                                        <hr>
                                    </div>
                                    <div class="li-desc">
                                        <p>Detail</p>
                                        <p>Harga</p>
                                    </div>
                                    <div class="li-desc">
                                        <p><?= $transaksi['nama_barang'] ?></p>
                                        <p><?= rupiah($transaksi['harga']) ?></p>
                                    </div>
                                    <div class="li-desc">
                                        <p>Ukuran</p>
                                        <p><?= $transaksi['ukuran'] ?></p>
                                    </div>
                                    <div class="li-desc">
                                        <p>jumlah</p>
                                        <p>x <?= $transaksi['jumlah_pembelian'] ?></p>
                                    </div>
                                    <div class="li-desc">
                                        <p>Total</p>
                                        <p><?= rupiah($transaksi['total_bayar']) ?></p>
                                    </div>

                                </div>
                                <hr>
                                <form action="" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="id" value="<?= $transaksi['id_transaksi'] ?>">
                                    <label for="">Keterangan :</label> <br>
                                    <input type="text" name="keterangan" class="keterangan" value="<?= $transaksi['keterangan'] ?>"></input>
                                    <br>
                                    <label for="ukuran" class="label-ukuran">Status :</label> <br>
                                    <select name="status" id="ukuran" required>
                                        <option value="" disabled selected>UBAH STATUS</option>
                                        <option value="DI TOLAK">DITOLAK</option>
                                        <option value="BELUM BAYAR">BELUM BAYAR</option>
                                        <option value="LUNAS">LUNAS</option>
                                        <option value="DI KEMAS">DI KEMAS</option>
                                        <option value="DI KIRIM">DI KIRIM</option>
                                        <option value="DI TERIMA">DI TERIMA</option>
                                    </select>
                                    <div class="btn-submit">
                                        <button class="submit" name="simpan">Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>



                    <!-- konten utama -->
                </div>
            </div>
        </div>
        <script src="script.js"></script>
</body>

</html>