<?php
//session
include "../include/functions.php";
include "../include/session.php";

session_start();
if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}
//sesion
// $produk = query("SELECT id_transaksi, transaksi.id_user, transaksi.id_produk, nama_barang, nama_lengkap, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
// FROM transaksi
// INNER JOIN produk ON transaksi.id_produk = produk.id_produk 
// INNER JOIN user ON transaksi.id_user = user.id_user 
// ORDER BY id_transaksi DESC");

// $produk = query("SELECT * FROM produk ORDER BY id_produk DESC");

if (isset($_GET['keyword'])) {
    $keyword2 = $_GET['keyword'];
    //pagenation
    $jumDataPerhalaman = 20;
    $jumlahData = count(query("SELECT id_transaksi, transaksi.id_user, transaksi.id_produk, nama_barang, nama_lengkap, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk 
INNER JOIN user ON transaksi.id_user = user.id_user 
WHERE nama_barang LIKE '%$keyword2%' OR nama_lengkap LIKE '%$keyword2%' OR info_status LIKE '%$keyword2%'"));
    $jumlahHalaman = ceil($jumlahData / $jumDataPerhalaman);
    if (isset($_GET['halaman'])) {
        $halamanAktif = $_GET['halaman'];
    } else {
        $halamanAktif = 1;
    }
    $awalData = ($jumDataPerhalaman * $halamanAktif) - $jumDataPerhalaman;
    $produk = query("SELECT id_transaksi, transaksi.id_user, transaksi.id_produk, nama_barang, nama_lengkap, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk 
INNER JOIN user ON transaksi.id_user = user.id_user 
WHERE nama_barang LIKE '%$keyword2%' OR nama_lengkap LIKE '%$keyword2%' OR info_status LIKE '%$keyword2%' ORDER BY id_transaksi DESC LIMIT $awalData,$jumDataPerhalaman");
}

if (!isset($_GET['keyword'])) {
    //pagination
    $keyword2 = '';
    $jumDataPerhalaman = 10;
    $jumlahData = count(query("SELECT id_transaksi, transaksi.id_user, transaksi.id_produk, nama_barang, nama_lengkap, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk 
INNER JOIN user ON transaksi.id_user = user.id_user 
ORDER BY id_transaksi DESC"));
    $jumlahHalaman = ceil($jumlahData / $jumDataPerhalaman);
    if (isset($_GET['halaman'])) {
        $halamanAktif = $_GET['halaman'];
    } else {
        $halamanAktif = 1;
    }
    //cara ternary
    // $halamanAktif = (isset($_GET['halaman'])) ? $_GET['halaman'] : 1;
    $awalData = ($jumDataPerhalaman * $halamanAktif) - $jumDataPerhalaman;
    $produk = query("SELECT id_transaksi, transaksi.id_user, transaksi.id_produk, nama_barang, nama_lengkap, ukuran, jumlah_pembelian, harga, total_bayar, gambar1,info_status
FROM transaksi
INNER JOIN produk ON transaksi.id_produk = produk.id_produk 
INNER JOIN user ON transaksi.id_user = user.id_user 
ORDER BY id_transaksi DESC LIMIT $awalData, $jumDataPerhalaman");
}


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <title>Admin dashboard</title>
    <link rel="stylesheet" href="../style/admin-style.css">
    <link rel="stylesheet" href="style-admin/product.css">
    <link rel="stylesheet" href="style-admin/pesanan.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>

<body>
    <div class="container">
        <div class="sidebar">
            <div class="head-icon">
                <a href="">Enryu Admin</a>
            </div>
            <div class="content">
                <a class="list-item" href="admin.php">
                    <i class="fa-solid fa-house-chimney"></i>
                    <p class="item-info hidden">HOME</p>
                </a>
                <a class="list-item" href="../">
                    <i class="fa-solid fa-globe"></i>
                    <p class="item-info hidden">Website</p>
                </a>
                <a class="list-item " href="admin-tambah-product.php">
                    <i class="fa-solid fa-star"></i>
                    <p class"item-info">TAMBAH ITEM</p>
                </a>
                <a class="list-item" href="admin-product.php">
                    <i class="fa-solid fa-store"></i>
                    <p class"item-info">PRODUCT</p>
                </a>
                <a class="list-item active" href="admin-pesanan.php">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p class"item-info">PESANAN</p>
                </a>
                <a class="list-item " href="admin-lunas.php">
                    <i class="fa-solid fa-coins"></i>
                    <p class"item-info">LUNAS</p>
                </a>
                <a class="list-item " href="admin-kemas.php">
                    <i class="fa-solid fa-boxes-packing"></i>
                    <p class"item-info">DI KEMAS</p>
                </a>
                <a class="list-item" href="admin-kirim.php">
                    <i class="fa-solid fa-truck-fast"></i>
                    <p class"item-info">DIKIRIM</p>
                </a>
            </div>
        </div>
        <div class="main">
            <div class="main-header">
                <div class="hams">
                    <i class="hams-button fa-solid fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="">Admin Dashboard</a>
                </div>
                <div class="account">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name">Dwimas</p>
                        <p class="acc-role">Super Admin</p>
                    </div>
                </div>
            </div>
            <div class="main-content main--product">
                <form action="" method="GET">
                    <input type="text" name="keyword" id="search" autofocus placeholder="masukkan keyword pencarian" autocomplete="off">
                    <button type="submit" class="search">Cari</button>
                </form>
                <h1>Daftar Pesanan</h1>
                <div id="container">
                    <table border="1" cellpadding="10" cellspacing="0">
                        <tr class="t-head">
                            <th>No.</th>
                            <th>Gambar</th>
                            <th>Nama Produk</th>
                            <th>Pembeli</th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Ukuran</th>
                            <th>status</th>
                            <th>Cek</th>
                        </tr>
                        <?php
                        if (isset($_GET['halaman'])) {
                            if ($_GET['halaman'] == 1) {
                                $j = 1;
                            } else {
                                $j = $_GET['halaman'] * 10 - 9;
                            }
                        } else {
                            $j = 1;
                        }
                        ?>
                        <?php $i = $j ?>
                        <?php foreach ($produk as $row) : ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td>
                                    <img src="../img/<?= $row["gambar1"] ?>" alt="gambar">
                                </td>
                                <td>
                                    <p><?= $row["nama_barang"]; ?></p>
                                </td>
                                <td>
                                    <p><?= $row["nama_lengkap"]; ?></p>
                                </td>
                                <td>
                                    <p><?= rupiah($row["harga"]); ?></p>
                                </td>
                                <td>
                                    <p><?= $row["jumlah_pembelian"]; ?></p>
                                </td>
                                <td>
                                    <p><?= $row["ukuran"]; ?></p>
                                </td>
                                <td>
                                    <p><?= $row["info_status"]; ?></p>
                                </td>
                                <td class="aksi">
                                    <a href="admin-detail-pesanan.php?id=<?= $row["id_transaksi"]; ?>">Detail</a>
                                </td>
                            </tr>
                            <?php $i++; ?>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
            <div class="page">
                <?php for ($i = 1; $i <= $jumlahHalaman; $i++) : ?>
                    <?php if ($i == $halamanAktif) : ?>
                        <a href="?halaman=<?= $i; ?><?php if (isset($_GET['keyword'])) {
                                                        echo '&keyword=' . $keyword2;
                                                    }; ?>" style="font-weight:bold; color:red;">
                            <?= $i; ?>
                        </a>
                    <?php else : ?>
                        <a href="?halaman=<?= $i; ?> <?php if (isset($_GET['keyword'])) {
                                                            echo '&keyword=' . $keyword2;
                                                        }; ?>">
                            <?= $i; ?>
                        </a>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</body>

</html>