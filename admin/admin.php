<?php
//session
session_start();
include "../include/functions.php";
include "../include/session.php";

if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}
//sesion

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <title>Admin dashboard</title>
    <link rel="stylesheet" href="../style/admin-style.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>

<body>
    <div class="container">
        <div class="sidebar">
            <div class="head-icon">
                <a href="">Enryu Admin</a>
            </div>
            <div class="content">
                <a class="list-item active" href="admin.php">
                    <i class="fa-solid fa-house-chimney"></i>
                    <p class="item-info hidden">HOME</p>
                </a>
                <a class="list-item" href="../">
                    <i class="fa-solid fa-globe"></i>
                    <p class="item-info hidden">Website</p>
                </a>
                <a class="list-item " href="admin-tambah-product.php">
                    <i class="fa-solid fa-star"></i>
                    <p class"item-info">TAMBAH ITEM</p>
                </a>
                <a class="list-item" href="admin-product.php">
                    <i class="fa-solid fa-store"></i>
                    <p class"item-info">PRODUCT</p>
                </a>
                <a class="list-item" href="admin-pesanan.php">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p class"item-info">PESANAN</p>
                </a>
                <a class="list-item " href="admin-lunas.php">
                    <i class="fa-solid fa-coins"></i>
                    <p class"item-info">LUNAS</p>
                </a>
                <a class="list-item " href="admin-kemas.php">
                    <i class="fa-solid fa-boxes-packing"></i>
                    <p class"item-info">DI KEMAS</p>
                </a>
                <a class="list-item" href="admin-kirim.php">
                    <i class="fa-solid fa-truck-fast"></i>
                    <p class"item-info">DIKIRIM</p>
                </a>
            </div>
        </div>
        <div class="main">
            <div class="main-header">
                <div class="hams">
                    <i class="hams-button fa-solid fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="">Admin Dashboard</a>
                </div>
                <div class="account">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name">Dwimas</p>
                        <p class="acc-role">Super Admin</p>
                    </div>
                </div>
            </div>
            <div class="main-content main--home">
                <h1>Selamat Datang Admin Enryu</h1>
                <p>Semoga harimu penuh dengan kebahagiaan</p>
            </div>
        </div>
    </div>
</body>

</html>