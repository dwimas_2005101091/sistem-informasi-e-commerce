<?php
//session
session_start();
include "../include/functions.php";
include "../include/session.php";

if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

if ($_SESSION['level'] !== 'SU_ADMIN') {
    header("Location: ../menu/login.php");
    exit;
}

//mengambil data id
$id = $_GET["id"];
//query
$produk = query("SELECT * FROM produk WHERE id_produk = $id")[0];


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <title>Admin dashboard</title>
    <link rel="stylesheet" href="../style/admin-style.css">
    <link rel="stylesheet" href="style-admin/tambah.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>
<?php
if (isset($_POST["submit"])) {
    if (ubah($_POST) > 0) {
        echo '<div><script>';
        echo "ubahOke();";
        echo '</script></div>';
    } else {
        echo '<div><script>';
        echo "UbahGagal();";
        echo '</script></div>';
    }
}

?>

<body>
    <div class="container">
        <div class="sidebar">
            <div class="head-icon">
                <a href="">Enryu Admin</a>
            </div>
            <div class="content">
                <a class="list-item" href="admin.php">
                    <i class="fa-solid fa-house-chimney"></i>
                    <p class="item-info hidden">HOME</p>
                </a>
                <a class="list-item" href="../">
                    <i class="fa-solid fa-globe"></i>
                    <p class="item-info hidden">Website</p>
                </a>
                <a class="list-item " href="admin-tambah-product.php">
                    <i class="fa-solid fa-star"></i>
                    <p class"item-info">TAMBAH ITEM</p>
                </a>
                <a class="list-item active" href="admin-product.php">
                    <i class="fa-solid fa-store"></i>
                    <p class"item-info">PRODUCT</p>
                </a>
                <a class="list-item" href="admin-pesanan.php">
                    <i class="fa-solid fa-cart-shopping"></i>
                    <p class"item-info">PESANAN</p>
                </a>
                <a class="list-item " href="admin-lunas.php">
                    <i class="fa-solid fa-coins"></i>
                    <p class"item-info">LUNAS</p>
                </a>
                <a class="list-item " href="admin-kemas.php">
                    <i class="fa-solid fa-boxes-packing"></i>
                    <p class"item-info">DI KEMAS</p>
                </a>
                <a class="list-item" href="admin-kirim.php">
                    <i class="fa-solid fa-truck-fast"></i>
                    <p class"item-info">DIKIRIM</p>
                </a>
            </div>
        </div>
        <div class="main">
            <div class="main-header">
                <div class="hams">
                    <i class="hams-button fa-solid fa-bars"></i>
                </div>
                <div class="logo">
                    <a href="">Admin Dashboard</a>
                </div>
                <div class="account">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name">Dwimas</p>
                        <p class="acc-role">Super Admin</p>
                    </div>
                </div>
            </div>
            <div class="main-content main--tambah">
                <h1>TAMBAH PRODUK</h1>
                <p class="inpo">isi data dengan baik dan benar</p>
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?= $produk["id_produk"]; ?>">
                    <input type="hidden" name="gambarLama1" value="<?= $produk["gambar1"]; ?>">
                    <input type="hidden" name="gambarLama2" value="<?= $produk["gambar2"]; ?>">
                    <input type="hidden" name="gambarLama3" value="<?= $produk["gambar3"]; ?>">
                    <input type="hidden" name="gambarLama4" value="<?= $produk["gambar4"]; ?>">
                    <label for="nama">Nama Produk : </label><br>
                    <input type="text" name="nama" id="nama" required value="<?= $produk["nama_barang"] ?>"><br>
                    <label for="harga">Harga : </label><br>
                    <input type="text" name="harga" id="harga" required value="<?= $produk["harga"] ?>"><br>
                    <label for="stok">Stok : </label><br>
                    <input type="text" name="stok" id="stok" required value="<?= $produk["stok"] ?>"><br>
                    <label for="desc_short">Deskripsi singkat :</label><br>
                    <input type="text" name="desc_short" id="desc_short" required value="<?= $produk["desc_short"] ?>"><br>
                    <label for="desc_long">Deskripsi Lengkap :</label><br>
                    <input type="text" name="desc_long" id="desc_long" required value="<?= $produk["desc_long"] ?>"><br>
                    <label for="gambar">Foto Utama :</label><br>
                    <img src="../img/<?= $produk["gambar1"] ?>" alt="">
                    <input type="file" name="gambar1" id="gambar1"><br>
                    <label for="gambar">Foto 2 :</label><br>
                    <img src="../img/<?= $produk["gambar2"] ?>" alt="">
                    <input type="file" name="gambar2" id="gambar2"><br>
                    <label for="gambar">Foto 3 :</label><br>
                    <img src="../img/<?= $produk["gambar3"] ?>" alt="">
                    <input type="file" name="gambar3" id="gambar3"><br>
                    <label for="gambar4">Foto 4 :</label><br>
                    <img src="../img/<?= $produk["gambar4"] ?>" alt="">
                    <input type="file" name="gambar4" id="gambar4"><br>
                    <button type="submit" name="submit">Ubah Data Produk!</button>
                </form>
            </div>
        </div>
    </div>

</body>

</html>