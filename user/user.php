<?php
session_start();
include "../include/functions.php";
include "../include/session.php";

$id  = $_COOKIE['id'];

$result = mysqli_query($conn, "SELECT * FROM user WHERE id_user = $id");
$row = mysqli_fetch_assoc($result);

if (!isset($_SESSION['login'])) {
    header("Location: ../menu/login.php");
    exit;
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Dwimas Budi Sulistyo">
    <meta name="description" content="Website untuk membeli sepatu-sepatu keren dengan harga terjangkau">
    <title>Enryu</title>
    <link rel="stylesheet" href="../style/user-style.css">
    <link rel="icon" type="image/x-icon" href="../assets/favicon.ico">
    <script src="https://kit.fontawesome.com/bc0c806829.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" charset="utf-8"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="../script.js"></script>
</head>
<?php
if (isset($_POST['simpan'])) {
    if (pengaturan($_POST, $id) > 0) {
        echo '<div><script>';
        echo "ubahAkunOke();";
        echo '</script></div>';
    } else {
        echo '<div><script>';
        echo "ubahAkunGagal();";
        echo '</script></div>';
    }
}

?>

<body>
    <!-- BAGIAN NAVIGASI ATAS -->
    <nav>
        <div class="logo-center">
            <div class="fav">
                <a href="../index.php">Enryu<span>.</span>co</a>
            </div>
            <div class="ham">
                <i class="ham-button fa-solid fa-bars"></i>
            </div>
        </div>

        <div class="navsub subright">
            <ul>
                <li>
                    <div class="searchBar">
                        <input type="text" id="search" name="search">
                        <button>
                            <i class="fa-solid fa-magnifying-glass"></i>
                        </button>
                    </div>
                </li>
                <li><a href="../menu/katalog">KATALOG</a></li>
                <li><a href="../menu/keranjang">KERANJANG</a></li>
                <li><a href="../menu/pesanan.php">PESANAN</a></li>
                <li><a href="user">AKUN</a></li>
                <li><a href="../menu/logout.php">LOG OUT</a></li>
            </ul>
        </div>
    </nav>
    <div class="spacer"></div>
    <div class="wrapper">
        <div class="container">
            <div class="account-setting">
                <div class="heading">
                    <h1>Informasi Akun</h1>
                </div>
                <div class="user">
                    <div class="acc-img">
                        <img src="../assets/admin.jpg" alt="">
                    </div>
                    <div class="acc-info">
                        <p class="acc-name"><?= $row["username"]; ?></p>
                        <p class="acc-role">Premium User</p>
                    </div>
                </div>
                <form method="post" action="" class="user-setting">
                    <label for="nama">
                        Nama Lengkap :<br>
                        <input type="text" name="nama" id="nama" value="<?= $row["nama_lengkap"]; ?>">
                    </label>
                    <label for="email">
                        Email : <br>
                        <input type="email" name="email" id="email" value="<?= $row["email"]; ?>">
                    </label>

                    <label for="no_hp">
                        Nomor handphone :<br>
                        <input type="text" name="no_hp" id="no_hp" value="<?= $row["no_handphone"]; ?>">
                    </label>
                    <label for="alamat">
                        Alamat Lengkap :<br>
                        <textarea name="alamat" id="alamat" cols="50" rows="10" required><?= $row["alamat"]; ?></textarea>
                    </label>
                    <div class="btn-simpan">
                        <button type="submit" name="simpan">Simpan</button>
                    </div>
                </form>
            </div>
            <!-- <div class="sideImage">
            <img src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/bdcb3b69213341.5b791e43d1b10.jpg" alt="pattern-img">
        </div> -->
        </div>
    </div>
    <div class="spacer"></div>
</body>

</html>